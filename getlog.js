const ethers = require("ethers");

(async () => {
  const provider = new ethers.providers.JsonRpcProvider("https://eth.llamarpc.com");
  const filterId = await provider.getLogs(
    {
        address: "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48",
        fromBlock:16266628
    }
  );
  console.log(filterId);
})();