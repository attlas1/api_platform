const jwt = require('jsonwebtoken');
const crypto = require('crypto');


const secret_key_encrypt_password =  "attlas123";
const secret_key_token = "attlas1234"
const rules = {
    access_resource: 1
}

const encrypt_password = (password)=>{
    const hmac = crypto.createHmac('sha256', secret_key_encrypt_password);
    return hmac.update(password).digest('hex');
};

const gen_access_resource = ({user_id})=>{
    return jwt.sign({
        user_id: user_id,
        rule: rules.access_resource
    },secret_key_token, {
        expiresIn: 60*30
    });
}
const verify_token = ({token})=>{
    try {
        const decoded = jwt.verify(token, secret_key_token);
        return decoded;
    } catch (e) {
        console.error(e)
        return undefined
    }
}


module.exports = {
    rules,
    encrypt_password,
    verify_token,
    gen_access_resource
}