const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId


const schema = new Schema({
  user_id: {
    type: ObjectId,
    index: true
  },
  value: {
    type: String,
    index: true
  }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

schema.index({user_id:1, value:1}, {unique:true})



module.exports =  mongoose.model('contracts', schema);;