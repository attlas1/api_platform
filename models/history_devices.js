const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Types.ObjectId


const schema = new Schema({
    user_id:{
        type: ObjectId,
        index: true
    },
    ip:{
        type: String,
        index: true
    },
    data: {
        type: Object
    }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});


module.exports =  mongoose.model('history_devices', schema);;