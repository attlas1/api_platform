const mongoose = require('mongoose');
const users = require('./users');
const addresses = require('./addresses');
const contracts = require('./contracts');
const history_devices = require('./history_devices');

mongoose.connect(`mongodb://${process.env.mongodb_user}:${process.env.mongodb_pass}@${process.env.mongodb_host}:${process.env.mongodb_port}/${process.env.mongodb_name}?authSource=admin`).then(() => console.log('Connected!')).catch((e)=>{
    console.error(`ERROR`, e);
    process.exit(1);
});


module.exports = {
    users,
    addresses,
    contracts,
    history_devices
}


