const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const schema = new Schema({
  email: {
    type: String,
    index: true,
    unique: true
  },
  password: {
    type: String,
    index: true
  }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});


module.exports =  mongoose.model('users', schema);;