const { ethers } = require("ethers");
// (i.e. ``http:/\/localhost:8545``)
const provider = new ethers.providers.JsonRpcProvider("https://eth.llamarpc.com");

// The provider also allows signing transactions to
// send ether and pay to change state within the blockchain.
// For this, we need the account signer...
//balance = await provider.getBalance("ethers.eth", )
filter = {
  topics: [
  // [ethers.utils.id("Transfer(address,address,uint256)"),
   // ethers.utils.id("DestroyedBlackFunds(address,uint256)")]
  ]
}
console.log(ethers.utils.id("Transfer(address,address,uint256)"))
provider.on(filter, (log, event) => {
  console.log(log, event)
  // Emitted whenever a DAI token transfer occurs
})

// You can also use an ENS name for the contract address
const daiAddress = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48" //dai.tokens.ethers.eth";

// The ERC-20 Contract ABI, which is a common contract interface
// for tokens (this is the Human-Readable ABI format)
const daiAbi = [
  // Some details about the token
  "function name() view returns (string)",
  "function symbol() view returns (string)",

  // Get the account balance
  "function balanceOf(address) view returns (uint)",

  // Send some of your tokens to someone else
  "function transfer(address to, uint amount)",

  // An event triggered whenever anyone transfers to someone else
  "event Transfer(address indexed from, address indexed to, uint amount)"
];

// The Contract object
const daiContract = new ethers.Contract(daiAddress, daiAbi, provider);

// daiContract.on("Transfer", (from, to, amount, event) => {
//     console.log(`${ from } sent ${amount} to ${ to}`);
//     // The event object contains the verbatim log data, the
//     // EventFragment and functions to fetch the block,
//     // transaction and receipt and event functions
// });

myAddress = "0x413404c82eF7aEe43BEC7fd3D70db2C0292487F1";
filter = daiContract.filters.Transfer(myAddress, null)
filter1 = daiContract.filters.Transfer(null, myAddress)


const main = async()=>{
    // Get the ERC-20 token name
    await daiContract.name()
    // 'Dai Stablecoin'

    // Get the ERC-20 token symbol (for tickers and UIs)
    await daiContract.symbol()
    // 'DAI'

    // Get the balance of an address
    // balance = await daiContract.balanceOf("ricmoo.firefly.eth")
    // // { BigNumber: "6026189439794538201631" }
    // console.log(balance)
    // // Format the DAI for displaying to the user
    // console.log(ethers.utils.formatUnits(balance, 18))
    // const data = await daiContract.queryFilter(filter, 4200218)
    // const data1 = await daiContract.queryFilter(filter1, 16200218)
    // console.log(data)
    // console.log(data1)


}
// Receive an event when ANY transfer occurs

// {
//   address: 'dai.tokens.ethers.eth',
//   topics: [
//     '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
//     null,
//     '0x0000000000000000000000008ba1f109551bd432803012645ac136ddd64dba72'
//   ]
// }

// Receive an event when that filter occurs
// daiContract.on(filter, (from, to, amount, event) => {
//     // The to will always be "address"
//     console.log(`I got ${amount} from ${ from }.`);
// });




main()