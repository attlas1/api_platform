
var provider = require('./provider');



const block_time = 15 * 1000;

module.exports = async({time, to_block})=>{
    let number =to_block || (await provider.getBlock("latest", false)).number;
   // let delta = timestamp*1000 - time.getTime();
    //let change = parseInt(delta / block_time);
   // number = number - change;
    while(true){
        let {timestamp} = await provider.getBlock(number, false)
        let delta = timestamp*1000 - time.getTime();
        if(delta <= 0){
            break;
        }
        if(delta > block_time * 2){
            let change = parseInt(delta / block_time);
            number = number - change;
        }else{
            number--
        }
    }
    return number
}