const ethers = require('ethers');
const moment = require('moment');

const provider = require('./provider');
const redis = require('../redis');
const {promisify} = require('util');


const asyncGet = promisify(redis.GET).bind(redis);
const asyncSet = promisify(redis.SET).bind(redis);
const block_time = 15 * 1000;

module.exports = async({time, address, to_block, balance=true})=>{
    const cache_time = JSON.parse(await asyncGet(`time-${time.getTime()}`));
    if(cache_time){
       console.log("Load from cache time", time);
        return {number: cache_time.number, balance:balance?ethers.utils.formatEther(await provider.getBalance(address, cache_time.number)): 0}
    }


    let number = to_block; //|| (await provider.getBlock("latest", false)).number
    //number = number - change;
    while(true){
        console.log("get cache for number", number);
        const cache = JSON.parse(await asyncGet(`key-${number}`))
        console.log("cache number", cache);
        let{timestamp} = cache || await provider.getBlock(number, false);
        (!cache)&&asyncSet(`key-${number}`, JSON.stringify({timestamp}));
        let delta = timestamp*1000 - time.getTime();
        console.log("Handle", number, delta, new Date(timestamp*1000))
        if(delta <= 0){
            break;
        }
        if(delta > block_time * 2){
            let change = parseInt(delta / block_time);
            number = number - change;
            console.log("change", change)
        }else{
            number--
           console.log("change", "one")
        }
    }
    (number!= to_block) && asyncSet(`time-${time.getTime()}`, JSON.stringify({number}));
    return {number, balance: balance?ethers.utils.formatEther(await provider.getBalance(address, number)):0}
}

const cache_block = async()=>{
    try {
        const to_block = await provider.getBlockNumber();
        const current_time = new Date();
        const end_time = moment(current_time).set({minute:0, second: 0, millisecond:0}).toDate();
        const start_time = moment(end_time).add(-24*30, 'hour');
        const points = [];
        for(let i=start_time; i<=end_time; i=moment(i).add(1, 'hour').toDate()){
          points.push(i);
        }
        await Promise.all(points.map(async(item)=>{
            return module.exports({time: moment(item).add(1, 'hour').toDate(), to_block, balance:false});
        }));
    }catch(e){
        console.error(`ERROR`, e);
    }
}


const start_cache = ()=>{
    cache_block();
    setInterval(()=>{
        cache_block();
    }, 15*60*1000)
}
start_cache();