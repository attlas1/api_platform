const { ethers } = require("ethers");

const provider = new ethers.providers.JsonRpcProvider("https://eth.llamarpc.com");

module.exports = provider;

