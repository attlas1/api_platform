const { ethers } = require("ethers");
const provider = require('./provider');

module.exports = async({contract, address, from_block, to_block, is_time=true})=>{

    const abi = [
        "function decimals() public view returns (uint8)",

        "function name() view returns (string)",

        "function symbol() view returns (string)",
        
        // Get the account balance
        "function balanceOf(address) view returns (uint)",

        // Send some of your tokens to someone else
        "function transfer(address to, uint amount)",

        // An event triggered whenever anyone transfers to someone else
        "event Transfer(address indexed from, address indexed to, uint amount)"
    ];

    // The Contract object
    const Contract = new ethers.Contract(contract, abi, provider);

    filter_from = Contract.filters.Transfer(address, null)
    filter_to = Contract.filters.Transfer(null, address)

    const [transaction_from, transaction_to, name, symbol, decimal] =  await Promise.all([
        ...[filter_from, filter_to].map((item)=>{
            return Contract.queryFilter(item, from_block, to_block)
        }),  
        Contract.name(), 
        Contract.symbol(),
        Contract.decimals()
    ]);

    console.log(transaction_from, transaction_to, name, symbol)
    const transactions = is_time? await Promise.all([...transaction_from.map((item)=>{
        return {
            ...item,
            type: "send",
            from: item.args.from,
            to: item.args.to,
            amount: ethers.utils.formatUnits(item.args.amount, decimal)
        }
    }), ...transaction_to.map((item)=>{
        return {
            ...item,
            type: "receive",
            from: item.args.from,
            to: item.args.to,
            amount: ethers.utils.formatEther(item.args.amount)
        }
    })].map(async(item)=>{
        const {timestamp} = await provider.getBlock(item.blockNumber);
        return {
            ...item,
            timestamp: timestamp*1000
        }
    })): [...transaction_from.map((item)=>{
        return {
            ...item,
            type: "send",
            from: item.args.from,
            to: item.args.to,
            amount: ethers.utils.formatEther(item.args.amount)
        }
    }), ...transaction_to.map((item)=>{
        return {
            ...item,
            type: "receive",
            from: item.args.from,
            to: item.args.to,
            amount: ethers.utils.formatEther(item.args.amount)
        }
    })]
    return {
        name, 
        symbol,
        transactions: transactions.sort((a, b)=>{
            return b.timestamp - a.timestamp;
        })
    }

}