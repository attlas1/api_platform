var Web3 = require('web3');
var provider = 'https://eth.llamarpc.com';
var web3Provider = new Web3.providers.HttpProvider(provider);
var web3 = new Web3(web3Provider);



const block_time = 15 * 1000;
const address = "0x19375894e6c741Cc545c67C992BA1D672a803b0e"

module.exports = async({time})=>{
    let {number, timestamp} = await web3.eth.getBlock("latest", false)
    let delta = timestamp*1000 - time.getTime();
    let change = parseInt(delta / block_time);
    number = number - change;
    while(true){
        let {timestamp} = await web3.eth.getBlock(number, false)
        let delta = timestamp*1000 - time.getTime();
     //   console.log("Handle", number, delta, new Date(timestamp*1000))
        if(delta <= 0){
            break;
        }
        if(delta > block_time * 2){
            let change = parseInt(delta / block_time);
            number = number - change;
     //       console.log("change", change)
        }else{
            number--
       //     console.log("change", "one")
        }
    }
    return {number, balance:web3.eth.getBalance(address, number)}
}


const test = async()=>{
    const times = [];
    const start = new Date();
    for(let i = 0; i< 720; i++){
        times.push(
            new Date(new Date().getTime() - i*3600*1000)
        )
    }
    console.log(times[-1])
    data = await Promise.all(times.map((item)=>{
        return module.exports({time:item})
    }));

    console.log(data.map((item)=>{
        return item.number
    }));
    const balances = await Promise.all(data.map((item)=>{
        return item.balance
    }))
    console.log(JSON.stringify(balances))
    console.log("delta", new Date() - start)
}

test()