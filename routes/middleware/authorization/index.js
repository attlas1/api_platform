

const {verify_token} = require('../../../utils');

module.exports =(req, res, next)=>{
    const token = req.token;
    const decoded = verify_token({token});
    if(decoded){
        req.user = decoded;
        next();
        return;
    }
    res.json({
        error:{
            code: "invalid_token"
        }
    });
}