const  express = require('express');
const addr = require('ipaddr.js');

const router = express.Router();

const {users:usersModel, history_devices} = require('../../models');

const {gen_access_resource, encrypt_password} = require('../../utils');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.post("/register", async(req, res)=>{
  const {email='', password=''} = req.body;
  try {
    if(!email || !password){
      res.json({
        error: {
          code: "invalid_param"
        }
      });
      return;
    }
    const exist = await usersModel.findOne({
      email: email.toLowerCase()
    });
    if(exist){
      res.json({
        error: {
          code: "email_exist"
        }
      });
      return;
    }
    const {_id:user_id} = await usersModel.create({
      email: email.toLowerCase(),
      password: encrypt_password(password)
    });
    res.json({
      data:{
        user_id
      }
    });
  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
});


router.post("/login", async(req, res)=>{
  const {browser, os, version, source} = req.useragent;
  const {email='', password=''} = req.body;
  try {
    const ip = addr.process(req.clientIp).octets.join('.');
   
    if(!email || !password){
      res.json({
        error: {
          code: "invalid_param"
        }
      });
      return;
    }
    const exist = await usersModel.findOne({
      email: email.toLowerCase(),
      password: encrypt_password(password)
    });
    if(!exist){
      res.json({
        error: {
          code: "invalid_email_or_password"
        }
      });
      return;
    }
    await history_devices.create({
      user_id: exist.id,
      ip: ip,
      data:{
        os: os,
        browser: browser,
        version, 
        source
      }
    });
    res.json({
      data:{
        token: gen_access_resource({user_id: exist._id})
      }
    });
  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
})

module.exports = router;
