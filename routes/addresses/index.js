const  express = require('express');
const moment = require('moment');
const router = express.Router();

const {addresses: addressesModel, contracts: contractsModel} = require('../../models');
const {rules} = require('../../utils');
const {provider, get_transactions, find_from_block, get_block_time} = require('../../eth');
const {authorization} = require('../middleware');


router.get("/", authorization, async(req, res)=>{
  const {user_id, rule} = req.user

  if(rule !== rules.access_resource){
    res.json({
        error: {
            code: "permission_denied"
        }
    });
    return;
  }
  try {
    const addresses = await addressesModel.find({
      user_id: user_id
    });
    res.json({
      data:{
        addresses
      }
    });
  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
});


router.get("/contracts", authorization, async(req, res)=>{
  const {user_id, rule} = req.user

  if(rule !== rules.access_resource){
    res.json({
        error: {
            code: "permission_denied"
        }
    });
    return;
  }
  try {
    const contracts = await contractsModel.find({
      user_id: user_id
    });
    res.json({
      data:{
        contracts
      }
    });
  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
});

router.get("/balance", authorization, async(req, res)=>{
  const {address=''} = req.query;
  const {user_id, rule} = req.user

  if(rule !== rules.access_resource){
    res.json({
        error: {
            code: "permission_denied"
        }
    });
    return;
  }
  try {
    const to_block = await provider.getBlockNumber();
    const current_time = new Date();
    const end_time = moment(current_time).set({minute:0, second: 0, millisecond:0}).toDate();
    const start_time = moment(end_time).add(-24*30, 'hour');
    const points = [];
    for(let i=start_time; i<=end_time; i=moment(i).add(1, 'hour').toDate()){
      points.push(i);
    }
    const value_points = await Promise.all(points.map(async(item)=>{
      const {balance, number} = await get_block_time({time: moment(item).add(1, 'hour').toDate(), address, to_block});
      return {
        time: item,
        number,
        balance
      }
    }));
    await addressesModel.updateOne({
        value: address,
        user_id: user_id
    },{
        value: address,
        user_id: user_id
    },{
        upsert: true
    });
    res.json({
      data:{
        points: value_points
      }
    });
  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
});


router.get("/transactions", authorization, async(req, res)=>{
  const {address='',contract=''} = req.query;
  const {user_id, rule} = req.user

  if(rule !== rules.access_resource){
    res.json({
        error: {
            code: "permission_denied"
        }
    });
    return;
  }
  try {
    const to_block = provider.getBlockNumber();
    const current_time = new Date();
    const delta_time = 30*24*3600*1000; // 30 day 
    const from_time = new Date(current_time.getTime() - delta_time);
    
    // find from_block
    const from_block = await find_from_block({time: from_time});

    const data = await get_transactions({contract, address, from_block, to_block});
    await addressesModel.updateOne({
      value:address,
      user_id
    },{
      value:address,
      user_id
    },{
      upsert: true
    });
    await contractsModel.updateOne({
      value:contract,
      user_id
    },{
      value:contract,
      user_id
    },{
      upsert: true
    })
    res.json({
        data: {
            ...data
        }
    })

  } catch (e) {
    console.error(`ERROR`, e);
    res.json({
      error: {
        code: "error_system"
      }
    });
  }
})

module.exports = router;
